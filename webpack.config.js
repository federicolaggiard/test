/**
 *
 * Webpack configuration script
 *
 */

'use strict';

module.exports = {
	entry: {
    	app: './test.js'
  	},
  	//output file
  output: {
    filename:      'build.js',
    path:          './dist'
  }
};
